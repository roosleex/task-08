<?xml version="1.0" encoding="UTF-8"?>
<!-- XSL transformation -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.nure.ua" version="1.0">

	<xsl:template match="Flowers">
		<html>
			<head>
				<title>Flowers</title>
				<style type="text/css">
					td {border: 1px solid black; padding: 5px;}
					table {border: 2px solid black;}
				</style>
			</head>
			<body>
				<table>
					<xsl:apply-templates select="Flower" />
				</table>
			</body>
		</html>
	</xsl:template>
<!-- 
	<xsl:template match="Flower">
		<tr>
			<td colspan="2">
				<xsl:value-of select="Flower" />
			</td>
		</tr>
		<xsl:apply-templates select="Flower" />
	</xsl:template>
 -->
</xsl:stylesheet>